#include <iostream>
#include <vector>
#include <utility>
#include <functional>
#include <type_traits>
#include <iterator>
#include <concepts>

#ifdef LOG
#define log(c) (std::cout << c)
#else
#define log(c)
#endif

// to be implementation provided
constexpr std::size_t N = 2 * sizeof(long long);

template <typename T>
  concept cheaply_copyable = 
    (std::is_trivially_copyable_v<T> && sizeof(T) <= N);

struct predictable_bool {
  bool value{};
  constexpr predictable_bool() = default;
  constexpr predictable_bool(bool v) : value(v) {}
  constexpr operator bool() const { return value; }
};

template <typename Predicate>
struct predictable {
  Predicate pred;
  constexpr predictable(Predicate&& p) : pred(std::move(p)) {}
  template <typename ...Args>
      requires std::predicate<Predicate, Args&&...>
    constexpr auto operator()(Args... args) -> predictable_bool {
      return bool(std::invoke(pred, (Args&&)(args)...));
    }
  template <typename ...Args>
      requires std::predicate<Predicate, Args&&...>
    constexpr auto operator()(Args... args) const -> predictable_bool {
      return bool(std::invoke(pred, (Args&&)(args)...));
    }
};

template <std::swappable T>
  constexpr bool swap_if(predictable_bool c, T& x, T& y) {
    log('p');
    if (c) std::swap(x, y);
      return bool(c);
  }

template <std::swappable T>
    requires (!cheaply_copyable<T>)
  constexpr bool swap_if(bool c, T& x, T& y) {
    return swap_if(predictable_bool(c), x, y);
  }

template <cheaply_copyable T>
  constexpr bool swap_if(bool c, T& x, T& y)
  {
    log('u');
    T tmp[2] = { x, y };
    y = tmp[1-c], x = tmp[c];
    return c;
  }

template <std::permutable I, typename Pred>
    requires std::sortable<I, Pred>
  I lomuto_part(I b, I e, Pred&& pred) {
    auto pivot = *(e - 1);
    I left = b;
    for (I right = b; right < e - 1; ++right) {
      left += swap_if(
        std::invoke((Pred&&)pred, *right, pivot), *left, *right);
    }
    std::swap(*left, *(e - 1));
    return left;
  }

template <std::permutable I>
  auto lomuto_part(I b, I e) {
    return lomuto_part(b, e, std::less<std::iter_value_t<I>>());
  }

template <std::permutable I, typename Pred>
    requires std::sortable<I, Pred>
  void quicksort(I begin, I end, Pred&& pred) {
    while (end - begin > 1) {
      I mid = lomuto_part(begin, end, (Pred&&)pred);
      quicksort(begin, mid, (Pred&&)pred);
      begin = mid + 1;
  } }

template <std::permutable I>
  void quicksort(I begin, I end) {
    quicksort(begin, end, std::less<std::iter_value_t<I>>());
  }

struct Big {
  char c;
  long tail[2];
  Big(char c) : c(c) {}
  bool operator<(Big const& o) const { return c < o.c; }
};

struct BigVec {
  std::vector<Big> v;
  template <std::size_t N> BigVec(const char (&s)[N]) : v(s, s+N-1) { }
  auto begin() { return v.begin(); }
  auto end() { return v.end(); }
  friend std::ostream& operator<<(std::ostream& o, BigVec s) {
    auto* buf = o.rdbuf();
    for (auto& b : s.v) buf->sputc(b.c);
    return o;
  }
};

int main() {
  {
    predictable_bool p;
    auto s = std::string("41965");
    std::cout << s << ' ';
    quicksort(s.begin(), s.end());   // not predicted
    std::cout << ' ' << s << ' ';
    quicksort(s.begin(), s.end(),    // predicted
      predictable([](int a, int b) { return a > b; }));
    std::cout << ' ' << s << ' ';
    quicksort(s.begin(), s.end(),    // predicted
      predictable(std::less<char>()));
    std::cout << ' ' << s << '\n';
  }
  {
    auto s = BigVec("41965");
    std::cout << s << ' ';
    quicksort(s.begin(), s.end());   // predicted
    std::cout << ' ' << s << ' ';
    quicksort(s.begin(), s.end(),    // predicted
      predictable([](Big const& a, Big const& b) { return b < a; }));
    std::cout << ' ' << s << ' ';
    quicksort(s.begin(), s.end(),    // predicted
      predictable(std::less<Big>()));
    std::cout << ' ' << s << '\n';
  }
}
