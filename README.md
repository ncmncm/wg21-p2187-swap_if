This is a repository for docs related to the ISO SC22/WG21 Standard C++
proposal P2187.

<http://cantrip.org/swap_if.pdf>

> ### `std::swap_if`, `std::predictable`
> 
> This paper proposes a new Standard Library primitive `swap_if`, currently used
> implicitly (but very suboptimally) in nearly half of Standard Library algorithms,
> and equally useful for users’ algorithms. Although `swap_if` is trivial to code
> correctly, current shipping compilers generate markedly sub-optimal code for
> naïve implementations.
>
> In addition, it proposes a means to indicate to Standard Library facilities that
> the results of an ordering predicate in a particular use have turned out to
> be predictable, so that a more appropriate variant of the algorithm may be
> substituted.
