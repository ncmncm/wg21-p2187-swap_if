#include <iostream>
#include <vector>
#include <utility>
#include <functional>
#include <type_traits>
#include <iterator>
#include <concepts>
#include <memory>
#include <cstring>
#include <initializer_list>

#ifdef LOG
#define log(c) (std::cout << c)
#else
#define log(c)
#endif

// customization point:
template <typename T>
  constexpr bool is_trivially_swappable_v = std::is_trivially_copyable_v<T>;

// certain non-trivial types also are trivially-swappable.
template <typename T, typename D>
  constexpr bool is_trivially_swappable_v<std::unique_ptr<T,D>> = true;

// to be implementation provided:
constexpr std::size_t N = 2 * sizeof(long long);

template <typename T>
  concept cheaply_swappable =
    (sizeof(T) <= N && is_trivially_swappable_v<T>);

template <typename I>
  concept cheaply_indirectly_swappable =
    cheaply_swappable<std::iter_value_t<I>>;

struct predictable_bool {
  bool value{};
  constexpr predictable_bool() = default;
  constexpr predictable_bool(bool v) : value(v) {}
  constexpr operator bool() const { return value; }
};

template <std::move_constructible Predicate>
  struct predictable {
    Predicate pred;

    constexpr predictable(Predicate&& p) : pred(std::move(p)) {}

    template <typename ...Args>
        requires std::predicate<Predicate&, Args&&...>
      constexpr auto operator()(Args&&... args)                  // nothing
        noexcept(std::is_nothrow_invocable_v<Predicate, Args...>)
          -> predictable_bool
      { return bool(std::invoke(pred, (Args&&)args...)); }

    template <typename ...Args>
        requires std::predicate<Predicate const&, Args&&...>
      constexpr auto operator()(Args&&... args)                     const
        noexcept(std::is_nothrow_invocable_v<Predicate const&, Args...>)
          -> predictable_bool
      { return bool(std::invoke(pred, (Args&&)args...)); }

    template <typename ...Args>
        requires std::predicate<Predicate&&, Args&&...>
      constexpr auto operator()(Args&&... args)                     &&
        noexcept(std::is_nothrow_invocable_v<Predicate&&, Args...>)
          -> predictable_bool
      { return bool(std::invoke(pred, (Args&&)args...)); }

    template <typename ...Args>
        requires std::predicate<Predicate const&&, Args&&...>
      constexpr auto operator()(Args&&... args)                     const&&
        noexcept(std::is_nothrow_invocable_v<Predicate const&&, Args...>)
          -> predictable_bool
      { return bool(std::invoke(pred, (Args&&)args...)); }
  };

template <std::swappable T>
  constexpr bool swap_if(predictable_bool c, T& x, T& y)
    noexcept(std::is_nothrow_swappable_v<T>)
  {
    log('p');
    if (c) std::swap(x, y);
    return bool(c);
  }

template <typename T>
    requires (cheaply_swappable<T> || std::swappable<T>)
  constexpr bool swap_if(bool c, T& x, T& y)
    noexcept(cheaply_swappable<T> || std::is_nothrow_swappable_v<T>)
  {
    if constexpr (cheaply_swappable<T>) {
      log('u');
      struct alignas(T) { char b[sizeof(T)]; } tmp[2];
      std::memcpy(tmp[0].b, &x, sizeof(x));
      std::memcpy(tmp[1].b, &y, sizeof(y));
      std::memcpy(&y, tmp[1-c].b, sizeof(x));
      std::memcpy(&x, tmp[c].b, sizeof(x));
      return c;
    } else return swap_if(predictable_bool(c), x, y);
  }

template <typename I>
    requires (std::indirectly_swappable<I,I>)
  constexpr bool iter_swap_if(predictable_bool c, I p, I q)
    noexcept(std::is_nothrow_invocable_v<decltype(std::iter_swap<I,I>), I, I>)
  {
    log('p');
    if (c) std::iter_swap(p, q);
    return bool(c);
  }

template <typename I>
    requires (cheaply_indirectly_swappable<I> ||
      std::indirectly_swappable<I,I>)
  constexpr bool iter_swap_if(bool c, I p, I q)
    noexcept(cheaply_indirectly_swappable<I> ||
      std::is_nothrow_swappable_v<std::iter_value_t<I>>)
  {
    if constexpr (cheaply_indirectly_swappable<I>) {
      return swap_if(c, *p, *q);
    } else return iter_swap_if(predictable_bool(c), p, q);
  }

template <std::permutable I, typename Pred>
    requires std::sortable<I, Pred>
  I lomuto_part(I b, I e, Pred&& pred) {
    --e;
    auto pivot = std::move(*e);
    I left = b;
    for (I right = b; right < e; ++right) {
      left += iter_swap_if(
        std::invoke(pred, *right, pivot), left, right);
    }
    *e = std::move(*left);
    *left = std::move(pivot);
    return left;
  }

template <std::permutable I, typename Pred>
    requires std::sortable<I, Pred>
  void quicksort(I begin, I end, Pred&& pred) {
    while (end - begin > 1) {
      I mid = lomuto_part(begin, end, (Pred&&)pred);
      quicksort(begin, mid, (Pred&&)pred);
      begin = mid + 1;
  } }

template <std::permutable I>
  void quicksort(I begin, I end) {
    quicksort(begin, end, std::less<std::iter_value_t<I>>());
  }

struct Big {
  char c;
  long long a, b;
  Big(char c) : c(c) {}
  bool operator<(Big const& o) const { return c < o.c; }
};

int main() {
  auto init = [](auto&& put) {
    for (auto s = "41965"; *s; ++s) put(*s);
  };
  auto out = [](auto& v, auto&& get)  {
    std::cout << ' ';
    for (auto& c : v) std::cout << get(c);
    std::cout << ' ';
  };
  {
    std::string s;
    init([&s](auto c) { s.push_back(c); });
    auto get = [](auto c) { return c; };
    out(s, get);
    quicksort(s.begin(), s.end());   // not predicted
    out(s, get);
    quicksort(s.begin(), s.end(),    // predicted
      predictable([](int a, int b) { return a > b; }));
    out(s, get);
    quicksort(s.begin(), s.end(),    // predicted
      predictable(std::less<char>()));
    out(s, get);
    std::cout << '\n';
  }
  {
    std::vector<int> s;
    init([&s](auto c) { s.push_back(c); });
    auto get = [](auto c) { return (char)c; };
    out(s, get);
    quicksort(s.begin(), s.end());   // not predicted
    out(s, get);
    quicksort(s.begin(), s.end(),    // predicted
      predictable([](int a, int b) { return a > b; }));
    out(s, get);
    quicksort(s.begin(), s.end(),    // predicted
      predictable(std::less<char>()));
    out(s, get);
    std::cout << '\n';
  }
  {
    std::vector<Big> s;
    init([&s](char c) { s.push_back({c}); });
    auto get = [](auto& b) { return b.c; };
    out(s, get);
    quicksort(s.begin(), s.end());   // predicted
    out(s, get);
    quicksort(s.begin(), s.end(),    // predicted
      predictable([](Big const& a, Big const& b) { return b < a; }));
    out(s, get);
    quicksort(s.begin(), s.end(),    // predicted
      predictable(std::less<Big>()));
    out(s, get);
    std::cout << '\n';
  }
  {
    auto s = std::vector<std::unique_ptr<char>>();
    static_assert(!std::is_trivially_copyable_v<
      std::remove_reference_t<decltype(*s.begin())>>);
    static_assert(is_trivially_swappable_v<
      std::remove_reference_t<decltype(*s.begin())>>);
    init([&s](char c) { s.push_back(std::make_unique<char>(c)); });
    auto get = [](auto& p) { return *p; };
    out(s, get);
    quicksort(s.begin(), s.end(),   // not predicted
      [](auto const& a, auto const& b) { return *a < *b; });
    out(s, get);
    quicksort(s.begin(), s.end(),    // predicted
      predictable([](auto& a, auto& b){ return *b < *a; }));
    out(s, get);
    std::cout << '\n';
  }
}
