CXX=g++
CXXFLAGS=-std=c++20 -DLOG

all: swap_if.pdf run

run: predictable 
	./predictable

%.pdf: %.md ; pandoc $< -o $@

%:%.cc ; ${CXX} ${CXXFLAGS} $< -o $@

post: swap_if.pdf ; scp swap_if.pdf ncm@cantrip.org:cantrip.org/

clean:; rm -f predictable swap_if.pdf

atril: swap_if.pdf; atril $< &
